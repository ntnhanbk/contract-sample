// SPDX-License-Identifier: MIT
// Creator: letieu
pragma solidity ^0.8.4;
import "./Stake.sol";

contract StakeHasFee is Stake {
    uint256 public chargeDeposit = 1 ether;
    uint256 public chargeWithdraw = 1 ether;
    uint256 public chargeClaim = 2 ether;

    bytes32 public constant CHARGE_PAYEE_ROLE = "CHARGE_PAYEE_ROLE";

    constructor(IERC20 _rewardCoin, uint256 _rewardPerWeek, uint256 _weeks, address[] memory _collections, uint256[] memory _ratio)
    Stake(_rewardCoin, _rewardPerWeek, _weeks, _collections, _ratio) {
        _grantRole(CHARGE_PAYEE_ROLE, msg.sender);
    }

    function setChargeFee(uint256 _deposit, uint256 _withdraw, uint256 _claim) onlyRole(DEFAULT_ADMIN_ROLE) public {
        chargeDeposit = _deposit;
        chargeWithdraw = _withdraw;
        chargeClaim = _claim;
    }

    function claimChargeFee() onlyRole(CHARGE_PAYEE_ROLE) public {
        payable(msg.sender).transfer(address(this).balance);
    }

    function deposit(address _collection, uint256[] memory ids) public payable override
    canStake(_collection) updateReward(msg.sender, _collection) whenNotPaused() {
        require(msg.value == chargeDeposit * ids.length, "not enough fund");
        super.deposit(_collection, ids);
    }

    function withdraw(address _collection, uint256[] memory ids) public payable override
    updateReward(msg.sender, _collection) {
        require(msg.value == chargeWithdraw * ids.length, "not enough fund");
        super.withdraw(_collection, ids);
    }

    function claimReward(address collection) public payable override {
        require(msg.value == chargeClaim, "not enough fund");
        super.claimReward(collection);
    }

    function claimAllReward() public payable override {
        require(msg.value == chargeClaim, "not enough fund");
        super.claimAllReward();
    }
}
