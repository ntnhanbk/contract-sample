// SPDX-License-Identifier: MIT
// Creator: letieu
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "hardhat/console.sol";

contract Stake is AccessControl, ReentrancyGuard, IERC721Receiver, Pausable {
    using SafeERC20 for IERC20;
    using SafeMath for uint256;

    uint256 constant oneWeek = 7*24*60*60;

    // SETTING VAR
    mapping(address => bool) public stakeCollections;
    mapping(address => uint256) ratio; // percentage
    IERC20 public rewardToken;
    uint256 rewardPerWeek;
    uint256 duration;

    // STATE VAR
    uint256 startTime;
    mapping(address => address[]) collectionToUsers;
    address[] collections;
    mapping(address => bool) collectionRewardUpdated;

    mapping(address => uint256) public lastTimeUpdated;
    mapping(address => uint256) public rewardPerToken;
    mapping(address => mapping(address => uint256)) public rewards;
    mapping(address => mapping(address => uint256)) public collectionToUserToStaked;
    mapping(address => mapping(address => uint256)) public collectionToUserToRewardPerTokenPaid;

    mapping(address => mapping(uint256 => address)) public nftToStaker;
    mapping(address => uint256) public nftStaked;

    constructor(IERC20 _rewardCoin, uint256 _rewardPerWeek, uint256 _weeks, address[] memory _collections, uint256[] memory _ratio) {
        rewardToken = _rewardCoin;
        rewardPerWeek = _rewardPerWeek;
        duration = _weeks.mul(oneWeek);
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        setCollections(_collections, _ratio);
    }

    ///////////////////////////
    // SETTING               //
    ///////////////////////////

    // GET
    function rewardPerSecond(address collection) public view returns (uint256) {
        return rewardPerWeek.mul(ratio[collection]).div(oneWeek).div(100);
    }

    // SET
    function setRewardAndDuration(uint256 _rewardPerWeek, uint256 _weeks) onlyRole(DEFAULT_ADMIN_ROLE) public{
        duration = _weeks.mul(7*24*60*60);
        rewardPerWeek = _rewardPerWeek;
    }

    function setCollections(address[] memory _collections, uint256[] memory _ratio) onlyRole(DEFAULT_ADMIN_ROLE) public {
        require(_collections.length == _ratio.length, "collection must be equal ratio len");
        uint256 totalPercent = 0;
        for (uint256 i = 0; i < _collections.length; i++) {
            totalPercent += _ratio[i];
        }
        require(totalPercent == 100, "totalPercent must be 100");
        for (uint256 i = 0; i < _collections.length; i++) {
            stakeCollections[_collections[i]] = true;
            ratio[_collections[i]] = _ratio[i];
        }
        collections = _collections;
    }

    function start() onlyRole(DEFAULT_ADMIN_ROLE) public {
        uint256 totalReward = duration.mul(rewardPerWeek).div(oneWeek);
        require(rewardToken.allowance(msg.sender, address(this)) >= totalReward, "not approved");
        rewardToken.safeTransferFrom(msg.sender, address(this), totalReward);

        startTime = block.timestamp;
    }

    function extend(uint256 _rewardPerWeek, uint256 _weeks) onlyRole(DEFAULT_ADMIN_ROLE) canExtend public {
        uint256 totalReward = _weeks * _rewardPerWeek;
        require(rewardToken.allowance(msg.sender, address(this)) >= totalReward, "not approved");
        rewardToken.safeTransferFrom(msg.sender, address(this), totalReward);

        rewardPerWeek = _rewardPerWeek;
        startTime = block.timestamp;
        duration = _weeks.mul(oneWeek);
        _updateLastTimeUpdated();
    }

    // call this function be for extend for each collection
    function updateAllUserReward(address collection) public {
        for (uint256 i = 0; i < collectionToUsers[collection].length; i++) {
            if (collectionToUserToStaked[collection][collectionToUsers[collection][i]] == 0) return;
            _updateUserReward(collectionToUsers[collection][i], collection);
        }
        collectionRewardUpdated[collection] = true;
    }

    function setPause(bool _isPause) onlyRole(DEFAULT_ADMIN_ROLE) public {
        if (_isPause) {
            _pause();
        } else {
            _unpause();
        }
    }

    ///////////////////////////
    // MODIFIER              //
    ///////////////////////////
    modifier updateReward(address user, address collection) {
        _updateUserReward(user, collection);
        _;
    }

    modifier canStake(address collection) {
        require(stakeCollections[collection] == true, "collection not accepted");
        require(rewardPerSecond(collection) > 0, "rewardPerSecond is 0");
        require(startTime.add(duration) >= block.timestamp, "finished");
        _;
    }

    modifier canExtend() {
        require(startTime.add(duration) < block.timestamp, "staking is running");
        bool isUpdatedReward = true;
        for (uint256 i = 0; i < collections.length; i++) {
            isUpdatedReward = collectionRewardUpdated[collections[i]];
        }
        require(isUpdatedReward, "Need to call updateAllUserReward for all collection");
        _;
    }

    ///////////////////////////
    // USER FUNCTION         //
    ///////////////////////////
    function deposit(address _collection, uint256[] memory ids) public payable virtual
    canStake(_collection) updateReward(msg.sender, _collection) whenNotPaused() {
        for (uint8 i = 0; i < ids.length; i++) {
            require(IERC721(_collection).ownerOf(ids[i]) == msg.sender, 'you not own this token');
            nftToStaker[_collection][ids[i]] = msg.sender;
            IERC721(_collection).safeTransferFrom(msg.sender, address(this), ids[i]);
        }
        collectionToUserToStaked[_collection][msg.sender] += ids.length;
        if (nftStaked[_collection] == 0) {
            collectionToUsers[_collection].push(msg.sender);
        }
        nftStaked[_collection] += ids.length;
    }

    function withdraw(address _collection, uint256[] memory ids) public payable virtual updateReward(msg.sender, _collection) {
        for (uint8 i = 0; i < ids.length; i++) {
            require(nftToStaker[_collection][ids[i]] == msg.sender, 'you not have permission to withdraw');
            nftToStaker[_collection][ids[i]] = address(0);
            IERC721(_collection).safeTransferFrom(address(this), msg.sender, ids[i]);
        }

        collectionToUserToStaked[_collection][msg.sender] -= ids.length;
        nftStaked[_collection] -= ids.length;
    }

    function claimReward(address collection) public payable virtual {
        (uint256 reward, uint256 currentRewardPerToken) = calculateReward(msg.sender, collection);
        rewards[collection][msg.sender] = 0;
        collectionToUserToRewardPerTokenPaid[collection][msg.sender] = currentRewardPerToken;

        rewardToken.safeTransfer(msg.sender, reward);
        emit ClaimReward(msg.sender, reward);
    }

    function claimAllReward() public payable virtual {
        for (uint256 i = 0; i < collections.length; i++) {
            claimReward(collections[i]);
        }
    }

    function calculateReward(address user, address collection) public view returns(uint256, uint256) {
        uint256 calculateTime = calculateRewardTime();
        if (collectionToUserToStaked[collection][user] == 0) return (
            rewards[collection][user],
            0
        );
        uint256 currentRewardPerToken = rewardPerToken[collection].add(
            rewardPerSecond(collection).mul(calculateTime.sub(lastTimeUpdated[collection])).div(nftStaked[collection])
        );
        uint256 newReward = calculateNewReward(collection, user, currentRewardPerToken);
        uint256 reward = rewards[collection][user].add(newReward);
        return (
            reward,
            currentRewardPerToken
        );
    }

    function calculateNewReward(address collection, address user, uint256 _rewardPerToken) internal view returns (uint256) {
        return collectionToUserToStaked[collection][user].mul(
            _rewardPerToken.sub(collectionToUserToRewardPerTokenPaid[collection][user])
        );
    }

    function calculateRewardTime() internal view returns (uint256) {
        uint256 calculateTime = block.timestamp;
        if (startTime.add(duration) < block.timestamp) {
            calculateTime = startTime.add(duration);
        }
        return calculateTime;
    }

    function isFinished() public view returns (bool) {
        return startTime.add(duration) < block.timestamp;
    }

    ///////////////////////////
    // OTHER                 //
    ///////////////////////////
    function onERC721Received(address operator, address from, uint256 tokenId, bytes memory data) external pure returns (bytes4) {
        return IERC721Receiver.onERC721Received.selector;
    }

    function _updateUserReward(address user, address collection) internal {
        if (nftStaked[collection] == 0) {
            rewardPerToken[collection] = 0;
        } else {
            uint256 calculateTime = calculateRewardTime();
            rewardPerToken[collection] = rewardPerToken[collection].add(
                rewardPerSecond(collection)
                .mul(calculateTime.sub(lastTimeUpdated[collection]))
                .div(nftStaked[collection])
            );
        }
        uint256 newReward = calculateNewReward(collection, user, rewardPerToken[collection]);
        rewards[collection][user] = rewards[collection][user].add(newReward);
        collectionToUserToRewardPerTokenPaid[collection][user] = rewardPerToken[collection];
        lastTimeUpdated[collection] = block.timestamp;
    }

    function _updateLastTimeUpdated() internal {
        for (uint256 i = 0; i < collections.length; i++) {
            lastTimeUpdated[collections[i]] = block.timestamp;
            collectionRewardUpdated[collections[i]] = false;
        }
    }

    event ClaimReward(address user, uint256 value);
}
