import { time, loadFixture, mine } from "@nomicfoundation/hardhat-network-helpers";
import { expect, use } from "chai";
import {ethers, network} from "hardhat";
import chaiAlmost from "chai-almost";
import {BigNumberish, ContractTransaction, Transaction} from "ethers";

use(chaiAlmost());

const rewardPerWeek = ethers.utils.parseEther('100');
const rewardPerSecond = rewardPerWeek.div(7*24*60*60);

function expectEqualEther(a: BigNumberish, b: BigNumberish) {
  return expect(+ethers.utils.formatEther(a)).to.be.almost(+ethers.utils.formatEther(b));
}

async function getClaimEvent(tx: ContractTransaction) {
  const r = await tx.wait();
  return r?.events?.find(e => e.event === 'ClaimReward')
}

describe("Stake", function () {
  async function deploy() {
    const [user1, user2, user3] = await ethers.getSigners();

    const ERC20 = await ethers.getContractFactory("ERC20Mock");
    const NFT = await ethers.getContractFactory("NFTMock");
    const Stake = await ethers.getContractFactory("Stake");

    const coin = await ERC20.deploy("mock", "mock");
    const nft1 = await NFT.deploy("mock", "mock");
    const nft2 = await NFT.deploy("mock", "mock");
    const stake = await Stake.deploy(coin.address, rewardPerWeek, 1, [nft1.address, nft2.address], [40, 60]);

    await coin.approve(stake.address, ethers.utils.parseEther("100").mul(3));

    // mint nft
    await nft1.mint(user2.address, 5);
    await nft1.mint(user3.address, 5);
    await nft2.mint(user3.address, 5);

    // approve
    await nft1.connect(user2).approve(stake.address, 1);
    await nft1.connect(user2).approve(stake.address, 2);
    await nft1.connect(user2).approve(stake.address, 3);

    await nft1.connect(user3).approve(stake.address, 6);
    await nft1.connect(user3).approve(stake.address, 7);
    await nft1.connect(user3).approve(stake.address, 8);

    await nft2.connect(user3).approve(stake.address, 1);
    await nft2.connect(user3).approve(stake.address, 2);
    await nft2.connect(user3).approve(stake.address, 3);

    // mint coin
    await coin.transfer(user1.address, ethers.utils.parseEther("100"));
    await coin.transfer(user2.address, ethers.utils.parseEther("100"));
    return { stake, nft1, nft2, coin };
  }

  describe("Deployment", function () {
    it("Should set the right token", async function () {
      const { stake, nft1, nft2, coin } = await loadFixture(deploy);
      await stake.start();

      expect(await stake.rewardToken()).to.equal(coin.address);
      expect(await stake.stakeCollections(nft1.address)).to.equal(true);
    });

    it("Should calculate rewardPerToken at time", async function () {
      const [user1, user2, user3] = await ethers.getSigners();
      const { stake, nft1, nft2, coin } = await loadFixture(deploy);
      await stake.start();
      const latestBlock = await ethers.provider.getBlock("latest")
      const timebase = latestBlock.timestamp + 1;

      const rewardPerSecNft1 = rewardPerSecond.mul(40).div(100);

      //========================================================== at 0s;
      await network.provider.send("evm_setNextBlockTimestamp", [timebase])
      await stake.connect(user2).deposit(nft1.address, [1, 2]);
      expect(await stake.rewardPerToken(nft1.address)).to.be.equal(0);


      //========================================================== at 2s;
      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 2])
      await stake.connect(user3).deposit(nft1.address, [6]);
      const r2 = await stake.rewardPerToken(nft1.address);
      expect(r2).to.be.equal(rewardPerSecNft1.mul(2).div(2));

      // at 2s, only user2 has reward, 2s = rewardPerSecond * 2
      const user2at2s = await stake.calculateReward(user2.address, nft1.address);
      expectEqualEther(user2at2s[0], rewardPerSecNft1.mul(2));


      //========================================================== at 12s;
      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 12])
      await stake.connect(user3).deposit(nft1.address, [7, 8]);
      const r12 = await stake.rewardPerToken(nft1.address);
      expect(r12).to.be.equal(r2.add(rewardPerSecNft1.mul(10).div(3)));

      // at 12s, user2 has more: 2/3 rewardPerSecond * 10s
      const user2at12s = await stake.calculateReward(user2.address, nft1.address);
      expectEqualEther(user2at12s[0], user2at2s[0].add(rewardPerSecNft1.mul(10).mul(2).div(3)))

      // at 12s, user3 has: 1/3 rewardPerSecond * 10s
      const user3at12s = await stake.calculateReward(user3.address, nft1.address);
      expectEqualEther(user3at12s[0], rewardPerSecNft1.mul(10).mul(1).div(3))


      //========================================================== at 13s;
      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 13])
      const tx = await stake.connect(user2).claimReward(nft1.address);
      const event = await getClaimEvent(tx);
      const rewarded = event?.args?.value;


      //========================================================== at 15s;
      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 15])
      await stake.connect(user2).withdraw(nft1.address, [1, 2]);
      const r15 = await stake.rewardPerToken(nft1.address);
      expect(r15).to.be.equal(r12.add(rewardPerSecNft1.mul(3).div(5)));

      // at 13s -> 15s, user2 have 2/5 nft, get 13s -> 15s reward (rewarded at 0s -> 13s)
      const user2at15s = await stake.calculateReward(user2.address, nft1.address);
      expectEqualEther(user2at15s[0], rewardPerSecNft1.mul(2).mul(15 - 13).div(5));

      // at 12s -> 15s, user3 have 3/5 nft
      const user3at15s = await stake.calculateReward(user3.address, nft1.address);
      expectEqualEther(user3at15s[0], user3at12s[0].add(rewardPerSecNft1.mul(3).mul(15 - 12).div(5)));

      //========================================================= Total at 15 must equal rewardPerSecond * 15
      expectEqualEther(rewarded.add(user2at15s[0]).add(user3at15s[0]), rewardPerSecNft1.mul(15));

      //========================================================= at 16s
      await mine()
      const user2at16s = await stake.calculateReward(user2.address, nft1.address);
      expectEqualEther(user2at16s[0], user2at15s[0]); // user2 not get more reward

      const user3at16s = await stake.calculateReward(user3.address, nft1.address);
      expectEqualEther(user3at16s[0], user3at15s[0].add(rewardPerSecNft1));

      await nft1.connect(user2).approve(stake.address, 1);
      //========================================================= at 20s
      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 20])
      await stake.connect(user2).deposit(nft1.address, [1]);

      //========================================================= at 22s
      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 22])
      await mine();
      const user2at22s = await stake.calculateReward(user2.address, nft1.address);
      expectEqualEther(user2at22s[0], user2at16s[0].add(rewardPerSecNft1.mul(2).div(4)));

      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 60 * 60 * 24 * 7])
      await mine();
      const user2at7d = await stake.calculateReward(user2.address, nft1.address);
      expect(await stake.isFinished()).to.equal(true);

      await mine();
      const user2AfterEnd = await stake.calculateReward(user2.address, nft1.address);
      const user3AfterEnd = await stake.calculateReward(user3.address, nft1.address);
      // reward should not be change after finished
      expectEqualEther(user2AfterEnd[0], user2at7d[0]);


      // total reward should be equal to total reward of collection
      const totalUser2Reward = rewarded.add(user2AfterEnd[0]);
      const totalUser3Reward = user3AfterEnd[0];
      const total = totalUser2Reward.add(totalUser3Reward);
      const remain = rewardPerWeek.mul(40).div(100).sub(total);
      expect(+ethers.utils.formatEther(remain)).to.lt(0.1);
    })

    it("Should transfer NFT exactly", async function () {
      const [user1, user2, user3] = await ethers.getSigners();
      const { stake, nft1, nft2, coin } = await loadFixture(deploy);
      await stake.start();

      // throw
      await expect(stake.connect(user3).deposit(nft1.address, [3])).to.be.rejectedWith(
        Error,
        "you not own this token"
      )

      await stake.connect(user2).deposit(nft1.address, [1, 2]);
      await stake.connect(user3).deposit(nft1.address, [6, 7]);
      await stake.connect(user3).deposit(nft2.address, [1, 2, 3]);

      expect(await stake.nftStaked(nft1.address)).to.equal(4);
      expect(await stake.nftStaked(nft2.address)).to.equal(3);
      expect(await stake.nftToStaker(nft1.address, 1)).to.equal(user2.address);
      expect(await stake.nftToStaker(nft1.address, 2)).to.equal(user2.address);
      expect(await stake.nftToStaker(nft1.address, 6)).to.equal(user3.address);
      expect(await stake.nftToStaker(nft1.address, 7)).to.equal(user3.address);
      expect(await stake.nftToStaker(nft2.address, 1)).to.equal(user3.address);
      expect(await stake.nftToStaker(nft2.address, 2)).to.equal(user3.address);
      expect(await stake.nftToStaker(nft2.address, 3)).to.equal(user3.address);


      expect(await nft1.ownerOf(1)).to.be.equal(stake.address);
      expect(await nft1.ownerOf(2)).to.be.equal(stake.address);
      expect(await nft1.ownerOf(6)).to.be.equal(stake.address);
      expect(await nft2.ownerOf(1)).to.be.equal(stake.address);

      expect(await nft1.balanceOf(stake.address)).to.be.equal(4);
      expect(await nft2.balanceOf(stake.address)).to.be.equal(3);
    });

    it("Should withdraw", async function () {
      const [user1, user2, user3] = await ethers.getSigners();
      const { stake, nft1, nft2, coin } = await loadFixture(deploy);
      await stake.start();

      await stake.connect(user2).deposit(nft1.address, [1, 2]);
      await stake.connect(user3).deposit(nft1.address, [6, 7]);
      await stake.connect(user3).deposit(nft2.address, [1, 2, 3]);

      await expect(stake.connect(user2).withdraw(nft1.address, [1, 2 ,3]))
        .to.be.rejectedWith(Error, "you not have permission to withdraw");

      await expect(stake.connect(user2).withdraw(nft1.address, [1, 2])).be.fulfilled;
      expect(await nft1.ownerOf(1)).to.be.equal(user2.address);
      expect(await nft1.ownerOf(2)).to.be.equal(user2.address);
    });

    it("Should extend", async () => {
      const [user1, user2, user3] = await ethers.getSigners();
      const { stake, nft1, nft2, coin } = await loadFixture(deploy);
      await stake.start();
      const latestBlock = await ethers.provider.getBlock("latest")
      const timebase = latestBlock.timestamp + 1;

      //========================================================== at 0s;
      await network.provider.send("evm_setNextBlockTimestamp", [timebase])
      await stake.connect(user2).deposit(nft1.address, [1, 2]);

      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 5])
      await stake.connect(user3).deposit(nft1.address, [6]);

      //========================================================== finished
      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 60 * 60 * 24 * 7])
      await mine();
      const user2at7d = await stake.calculateReward(user2.address, nft1.address);
      expect(await stake.isFinished()).to.equal(true);

      // provide for extend
      const extendTimeBase = timebase + 60 * 60 * 24 * 7 + 100;
      const newRewardPerWeek = ethers.utils.parseEther('200')
      const newRewardPerSecond = newRewardPerWeek.div(7*24*60*60);
      const newRewardPerSecNft1 = newRewardPerSecond.mul(40).div(100);

      await network.provider.send("evm_setNextBlockTimestamp", [extendTimeBase - 3])
      await stake.updateAllUserReward(nft1.address);
      await stake.updateAllUserReward(nft2.address);
      await coin.approve(stake.address, newRewardPerWeek.mul(2));
      const user2Reward = await stake.rewards(nft1.address, user2.address);
      expectEqualEther(user2Reward, user2at7d[0]);

      // extend
      await network.provider.send("evm_setNextBlockTimestamp", [extendTimeBase])
      await stake.extend(newRewardPerWeek, 2);
      const user2AtBase = await stake.calculateReward(user2.address, nft1.address);
      expectEqualEther(user2AtBase[0], user2Reward);

      // should continue to calculate reward
      await network.provider.send("evm_setNextBlockTimestamp", [extendTimeBase + 10])
      await mine();
      const user2At10s = await stake.calculateReward(user2.address, nft1.address);
      expectEqualEther(user2At10s[0].sub(user2AtBase[0]), newRewardPerSecNft1.mul(10).mul(2).div(3))
    })

    it("Should claimReward", async function() {
      const [user1, user2, user3] = await ethers.getSigners();
      const { stake, nft1, nft2, coin } = await loadFixture(deploy);

      const user2BalanceBefore = await coin.balanceOf(user2.address);

      await stake.start();
      const latestBlock = await ethers.provider.getBlock("latest")
      const timebase = latestBlock.timestamp + 1;

      await network.provider.send("evm_setNextBlockTimestamp", [timebase])
      await stake.connect(user2).deposit(nft1.address, [1, 2]);

      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 2])
      await stake.connect(user3).deposit(nft1.address, [6]);

      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 12])
      await stake.connect(user3).deposit(nft1.address, [7, 8]);

      await network.provider.send("evm_setNextBlockTimestamp", [timebase + 13])
      const tx = await stake.connect(user2).claimReward(nft1.address);
      const event = await getClaimEvent(tx);
      const rewarded = event?.args?.value;

      const user2BalanceAfter = await coin.balanceOf(user2.address);
      expect(user2BalanceBefore.add(rewarded)).to.be.equal(user2BalanceAfter);
    })
  });
});
