import { time, loadFixture, mine } from "@nomicfoundation/hardhat-network-helpers";
import { expect, use } from "chai";
import {ethers, network} from "hardhat";
import chaiAlmost from "chai-almost";
import {BigNumberish, ContractTransaction, Transaction} from "ethers";

use(chaiAlmost());

const rewardPerWeek = ethers.utils.parseEther('100');
const rewardPerSecond = rewardPerWeek.div(7*24*60*60);

function expectEqualEther(a: BigNumberish, b: BigNumberish) {
  return expect(+ethers.utils.formatEther(a)).to.be.almost(+ethers.utils.formatEther(b));
}

async function getClaimEvent(tx: ContractTransaction) {
  const r = await tx.wait();
  return r?.events?.find(e => e.event === 'ClaimReward')
}

describe("StakeHasFee", function () {
  async function deploy() {
    const [user1, user2, user3] = await ethers.getSigners();

    const ERC20 = await ethers.getContractFactory("ERC20Mock");
    const NFT = await ethers.getContractFactory("NFTMock");
    const Stake = await ethers.getContractFactory("StakeHasFee");

    const coin = await ERC20.deploy("mock", "mock");
    const nft1 = await NFT.deploy("mock", "mock");
    const nft2 = await NFT.deploy("mock", "mock");
    const stake = await Stake.deploy(coin.address, rewardPerWeek, 3, [nft1.address, nft2.address], [40, 60]);

    // setup stake
    await coin.approve(stake.address, ethers.utils.parseEther("100").mul(3));

    // mint nft
    await nft1.mint(user2.address, 5);
    await nft1.mint(user3.address, 5);
    await nft2.mint(user3.address, 5);

    // approve
    await nft1.connect(user2).approve(stake.address, 1);
    await nft1.connect(user2).approve(stake.address, 2);
    await nft1.connect(user2).approve(stake.address, 3);

    await nft1.connect(user3).approve(stake.address, 6);
    await nft1.connect(user3).approve(stake.address, 7);
    await nft1.connect(user3).approve(stake.address, 8);

    await nft2.connect(user3).approve(stake.address, 1);
    await nft2.connect(user3).approve(stake.address, 2);
    await nft2.connect(user3).approve(stake.address, 3);

    // mint coin
    await coin.transfer(user1.address, ethers.utils.parseEther("100"));
    await coin.transfer(user2.address, ethers.utils.parseEther("100"));
    return { stake, nft1, nft2, coin };
  }

  describe("Deployment", function () {
    it("Should have default charge fee", async function () {
      const {stake, nft1, nft2, coin} = await loadFixture(deploy);

      expect(await stake.chargeDeposit()).to.equal(ethers.utils.parseEther("1"));
      expect(await stake.chargeWithdraw()).to.equal(ethers.utils.parseEther("1"));
      expect(await stake.chargeClaim()).to.equal(ethers.utils.parseEther("2"));
    });

    it("Should set the charge fee", async function () {
      const {stake, nft1, nft2, coin} = await loadFixture(deploy);
      await stake.setChargeFee(
        ethers.utils.parseEther("1.5"),
        ethers.utils.parseEther("1.5"),
        ethers.utils.parseEther("2.5")
      );

      expect(await stake.chargeDeposit()).to.equal(ethers.utils.parseEther("1.5"));
      expect(await stake.chargeWithdraw()).to.equal(ethers.utils.parseEther("1.5"));
      expect(await stake.chargeClaim()).to.equal(ethers.utils.parseEther("2.5"));
    });

    it("Should set the payee", async function () {
      const [user1, user2, user3] = await ethers.getSigners();
      const {stake, nft1, nft2, coin} = await loadFixture(deploy);
      const payeeRole = await stake.CHARGE_PAYEE_ROLE();
      await stake.grantRole(payeeRole, user2.address);
      expect(await stake.hasRole(payeeRole, user2.address)).to.be.equal(true);
    });

    it("Should take fee per action", async function () {
      const [user1, user2, user3] = await ethers.getSigners();
      const {stake, nft1, nft2, coin} = await loadFixture(deploy);
      const payeeRole = await stake.CHARGE_PAYEE_ROLE();
      await stake.grantRole(payeeRole, user2.address);
      await stake.start();

      await stake.connect(user2).deposit(nft1.address, [1, 2], {
        value: ethers.utils.parseEther("2")
      });

      await mine();
      await mine();
      await mine();

      await stake.connect(user2).withdraw(nft1.address, [1, 2], {
        value: ethers.utils.parseEther("2")
      });

      await stake.connect(user2).claimReward(nft1.address, {
        value: ethers.utils.parseEther("2")
      });

      expect(await stake.provider.getBalance(stake.address)).to.be.equal(ethers.utils.parseEther("6"))

      await stake.connect(user2).claimChargeFee();
      expect(await stake.provider.getBalance(stake.address)).to.be.equal(ethers.utils.parseEther("0"))
    });
  });
});
