# Smart contract for staking.corgistudio.io

[Reference Algorithm](https://www.youtube.com/watch?v=iNZWMj4USUM&list=PLC8nVXqYbsNrFQQ8ehbR41vk_vle8rmd_&index=24&ab_channel=SmartContractProgrammer)

# How to use

deploy:
```js
import { abi, bytecode } from "abis/StakeHasFee.json";

factory = new ContractFactory(abi, bytecode, signer)
const rewardPerWeek = ethers.utils.parseEther('10');
const weeks = 3;
const stake = await factory.deploy(coin.address, rewardPerWeek, weeks);
// set collection list and ratio
await stake.setCollections([nft1.address, nft2.address], [40, 60]);
```

start staking
```js
await stake.start();
```